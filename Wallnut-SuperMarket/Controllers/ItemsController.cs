﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Wallnut_SuperMarket.Models;
using System.IO;


namespace Wallnut_SuperMarket.Controllers
{
    public class ItemsController : Controller
    {
        private WallnutDatabaseEntities6 db = new WallnutDatabaseEntities6();

        // Admin
        // GET: Items
        public async Task<ActionResult> Index()
        {
            WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
            WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();

            List<Counter> counterList = await counterDb.Counters.ToListAsync();
            List<Category> categoryList = await categoryDb.Categories.ToListAsync();

            ViewData["counterList"] = counterList;
            ViewData["categoryList"] = categoryList;

            return View(await db.Items.ToListAsync());
        }

        // Admin
        // GET: Items/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = await db.Items.FindAsync(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
            WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();

            List<Counter> counterList = await counterDb.Counters.ToListAsync();
            List<Category> categoryList = await categoryDb.Categories.ToListAsync();

            ViewData["counterList"] = counterList;
            ViewData["categoryList"] = categoryList;

            return View(item);
        }

        // Admin
        // GET: Items/Create
        public async Task<ActionResult> Create()
        {
            WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
            WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();

            List<Counter> counterList = await counterDb.Counters.ToListAsync();
            List<Category> categoryList = await categoryDb.Categories.ToListAsync();

            ViewData["counterList"] = counterList;
            ViewData["categoryList"] = categoryList;

            return View();
        }

        // Admin
        // POST: Items/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Item item)
        {
            string fileName = Path.GetFileNameWithoutExtension(item.ImageFile.FileName);
            string extension = Path.GetExtension(item.ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            item.picture = "~/ImageItem/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/ImageItem/"), fileName);
            item.ImageFile.SaveAs(fileName);

            db.Items.Add(item);
            db.SaveChanges();
            ModelState.Clear();

            return RedirectToAction("Index");
        }

        // Admin
        // GET: Items/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = await db.Items.FindAsync(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
            WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();

            List<Counter> counterList = await counterDb.Counters.ToListAsync();
            List<Category> categoryList = await categoryDb.Categories.ToListAsync();

            ViewData["counterList"] = counterList;
            ViewData["categoryList"] = categoryList;

            return View(item);
        }

        // Admin
        // GET: Items/AddStock/5
        public async Task<ActionResult> AddStock(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = await db.Items.FindAsync(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
            WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();

            List<Counter> counterList = await counterDb.Counters.ToListAsync();
            List<Category> categoryList = await categoryDb.Categories.ToListAsync();

            ViewData["counterList"] = counterList;
            ViewData["categoryList"] = categoryList;

            return View(item);
        }

        // Admin
        // POST: Items/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,name,counterId,categoryId,brand,rating,picture,stock")] Item item)
        {
            if (ModelState.IsValid)
            {
                db.Entry(item).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
            WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();

            List<Counter> counterList = await counterDb.Counters.ToListAsync();
            List<Category> categoryList = await categoryDb.Categories.ToListAsync();

            ViewData["counterList"] = counterList;
            ViewData["categoryList"] = categoryList;

            return View(item);
        }

        // Admin
        // POST: Items/AddStock/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddStock([Bind(Include = "id,name,counterId,categoryId,brand,rating,picture,stock")] Item item)
        {
            if (ModelState.IsValid)
            {
                Item itemFromDb = await db.Items.FindAsync(item.id);
                itemFromDb.stock = itemFromDb.stock + item.stock;

                db.Entry(itemFromDb).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
            WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();

            List<Counter> counterList = await counterDb.Counters.ToListAsync();
            List<Category> categoryList = await categoryDb.Categories.ToListAsync();

            ViewData["counterList"] = counterList;
            ViewData["categoryList"] = categoryList;

            return View(item);
        }

        // Admin
        // GET: Items/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = await db.Items.FindAsync(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
            WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();

            List<Counter> counterList = await counterDb.Counters.ToListAsync();
            List<Category> categoryList = await categoryDb.Categories.ToListAsync();

            ViewData["counterList"] = counterList;
            ViewData["categoryList"] = categoryList;

            return View(item);
        }

        // Admin
        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Item item = await db.Items.FindAsync(id);
            db.Items.Remove(item);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
