﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Wallnut_SuperMarket.Models;

namespace Wallnut_SuperMarket.Controllers
{
    public class CountersController : Controller
    {
        private WallnutDatabaseEntities2 db = new WallnutDatabaseEntities2();

        // Admin
        // GET: Counters
        public async Task<ActionResult> Index()
        {
            return View(await db.Counters.ToListAsync());
        }

        // Admin
        // GET: Counters/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Counter counter = await db.Counters.FindAsync(id);
            if (counter == null)
            {
                return HttpNotFound();
            }
            return View(counter);
        }

        // Admin
        // GET: Counters/Create
        public ActionResult Create()
        {
            return View();
        }

        // Admin
        // POST: Counters/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,counter1,description")] Counter counter)
        {
            if (ModelState.IsValid)
            {
                db.Counters.Add(counter);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(counter);
        }

        // Admin
        // GET: Counters/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Counter counter = await db.Counters.FindAsync(id);
            if (counter == null)
            {
                return HttpNotFound();
            }
            return View(counter);
        }

        // Admin
        // POST: Counters/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,counter1,description")] Counter counter)
        {
            if (ModelState.IsValid)
            {
                db.Entry(counter).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(counter);
        }

        // Admin
        // GET: Counters/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Counter counter = await db.Counters.FindAsync(id);
            if (counter == null)
            {
                return HttpNotFound();
            }
            return View(counter);
        }

        // Admin
        // POST: Counters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Counter counter = await db.Counters.FindAsync(id);
            db.Counters.Remove(counter);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
