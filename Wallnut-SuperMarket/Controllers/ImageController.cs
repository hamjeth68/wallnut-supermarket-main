﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wallnut_SuperMarket.Models;

namespace Wallnut_SuperMarket.Controllers
{
    public class ImageController : Controller
    {
        private WallnutDatabaseEntities5 db = new WallnutDatabaseEntities5();

        // Admin
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        // Admin
        [HttpPost]
        public ActionResult Add(Image imageModel)
        {
            string fileName = Path.GetFileNameWithoutExtension(imageModel.ImageFile.FileName);
            string extension = Path.GetExtension(imageModel.ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            imageModel.path = "~/Image/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/"), fileName);
            imageModel.ImageFile.SaveAs(fileName);

            db.Images.Add(imageModel);
            db.SaveChanges();
            ModelState.Clear();
            return View();
        }

        // Admin
        [HttpGet]
        public ActionResult View(int id)
        {
            Image imageModel = new Image();
            imageModel = db.Images.Where(x => x.id == id).FirstOrDefault();
         
            return View(imageModel);
        }
    }
}