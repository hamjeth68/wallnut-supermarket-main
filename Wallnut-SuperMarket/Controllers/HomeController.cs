﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Wallnut_SuperMarket.Models;

//my one
//home
//new
namespace Wallnut_SuperMarket.Controllers
{
    public class HomeController : Controller
    {
        private WallnutDatabaseEntities db = new WallnutDatabaseEntities();

        public async Task<ActionResult> Page()
        {
            if (Session["user"] != null)
            {
                User userObj = (User)Session["user"];

                if (userObj.type.Equals("CUSTOMER"))
                {
                    return RedirectToAction("Index");
                }
                else if (userObj.type.Equals("SALES_MANAGER"))
                {
                    return View("SALES_MANAGER_HOME");
                }
                else if (userObj.type.Equals("COUNTER_MANAGER"))
                {
                    return View("COUNTER_MANAGER_HOME");
                }
                else if (userObj.type.Equals("SALES_PERSON"))
                {
                    return View("SALES_PERSON_HOME");
                }
                else if (userObj.type.Equals("DELIVERY_PERSON"))
                {
                    return View("DELIVERY_PERSON_HOME");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        // Home
        public async Task<ActionResult> Index()
        {
            WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
            WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();
            WallnutDatabaseEntities6 itemDb = new WallnutDatabaseEntities6();

            List<Counter> counterList = await counterDb.Counters.ToListAsync();
            List<Category> categoryList = await categoryDb.Categories.ToListAsync();
            List<Item> itemList = await itemDb.Items.ToListAsync();

            ViewData["counterList"] = counterList;
            ViewData["categoryList"] = categoryList;
            ViewData["itemList"] = itemList;

            return View();
        }

        // Users
        // GET: Home/AddCart
        public async Task<ActionResult> AddCart(int? id)
        {
            WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
            WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();
            WallnutDatabaseEntities6 itemDb = new WallnutDatabaseEntities6();

            List<Counter> counterList = await counterDb.Counters.ToListAsync();
            List<Category> categoryList = await categoryDb.Categories.ToListAsync();
            List<Item> itemList = await itemDb.Items.ToListAsync();

            if (Session["cartList"] != null)
            {
                List<Cart> cartList = (List<Cart>)Session["cartList"];
                foreach (Cart cartObj in cartList)
                {
                    if (cartObj.itemId == id)
                    {
                        ViewData["id"] = id;
                        ViewData["counterList"] = counterList;
                        ViewData["categoryList"] = categoryList;
                        ViewData["itemList"] = itemList;
                        ViewData["Status"] = "Requested item is already present!!! You cannot add this item";
                        return View("AddCart");
                    }
                }
            }

            ViewData["id"] = id;
            ViewData["counterList"] = counterList;
            ViewData["categoryList"] = categoryList;
            ViewData["itemList"] = itemList;
            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddCart([Bind(Include = "id, itemId, qty")] Cart cart)
        {
            WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
            WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();
            WallnutDatabaseEntities6 itemDb = new WallnutDatabaseEntities6();

            List<Counter> counterList = await counterDb.Counters.ToListAsync();
            List<Category> categoryList = await categoryDb.Categories.ToListAsync();
            List<Item> itemList = await itemDb.Items.ToListAsync();

            if (Session["cartList"] == null)
            {
                // first time
                foreach (var item in itemList)
                {
                    if (item.id == cart.itemId)
                    {
                        // Check if requested qty is higher than the stock
                        if (cart.qty > item.stock)
                        {
                            ViewData["id"] = cart.itemId;
                            ViewData["counterList"] = counterList;
                            ViewData["categoryList"] = categoryList;
                            ViewData["itemList"] = itemList;
                            ViewData["Status"] = "Requested quantity is high!!! Stock available " + item.stock;
                            return View("AddCart");
                        }
                        List<Cart> cartList = new List<Cart>();
                        cart.id = (int)cart.itemId;
                        cartList.Add(cart);
                        Session["cartList"] = cartList;
                    }
                }
                return RedirectToAction("Index");
            }
            else
            {
                // cart session already present
                List<Cart> cartList = (List<Cart>)Session["cartList"];
                foreach (Cart cartObj in cartList)
                {
                    if (cartObj.itemId == cart.itemId)
                    {
                        ViewData["id"] = cart.itemId;
                        ViewData["counterList"] = counterList;
                        ViewData["categoryList"] = categoryList;
                        ViewData["itemList"] = itemList;
                        ViewData["Status"] = "Requested item is already present!!! You cannot add this item";
                        return View("AddCart");
                    }
                    else
                    {
                        // Check if requested qty is higher than the stock
                        foreach (var item in itemList)
                        {
                            if (cart.itemId == item.id && cart.qty > item.stock)
                            {
                                ViewData["id"] = cart.itemId;
                                ViewData["counterList"] = counterList;
                                ViewData["categoryList"] = categoryList;
                                ViewData["itemList"] = itemList;
                                ViewData["Status"] = "Requested quantity is high!!! Stock available " + item.stock;
                                return View("AddCart");
                            }
                            else if(cart.itemId == item.id && cart.qty <= item.stock)
                            {
                                
                            }
                        }
                    }
                }
                cart.id = (int)cart.itemId;
                cartList.Add(cart);
                Session["cartList"] = cartList;
                return RedirectToAction("Index");
            }
        }

        // Admin
        // GET: Home/AdminHome
        public ActionResult AdminHome()
        {
            return View();
        }

        // Nothing
        // GET: Home/Register
        public ActionResult Register()
        {
            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register([Bind(Include = "id,username,password,email,phone,address")] User user)
        {
            WallnutDatabaseEntities14 walletdb = new WallnutDatabaseEntities14();
            if (ModelState.IsValid)
            {
                List<User> userList = await db.Users.ToListAsync();
                foreach (User userObj in userList)
                {
                    if (user.username.Equals(userObj.username))
                    {
                        ViewData["Status"] = "Username " + userObj.username + " is already taken!!!";
                        return View("Register");
                    }
                }

                string encodedPassword = EncodePassword(user.password);
                user.id = 0;
                user.password = encodedPassword;
                user.type = "CUSTOMER";

                db.Users.Add(user);
                await db.SaveChangesAsync();

                Wallet wallet = new Wallet();
                wallet.id = 0;
                wallet.userId = user.id;
                wallet.amount = 0;

                walletdb.Wallets.Add(wallet);
                await walletdb.SaveChangesAsync();

                Session["loggedIn"] = true;
                Session["User"] = user;

                WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
                WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();
                WallnutDatabaseEntities6 itemDb = new WallnutDatabaseEntities6();

                List<Counter> counterList = await counterDb.Counters.ToListAsync();
                List<Category> categoryList = await categoryDb.Categories.ToListAsync();
                List<Item> itemList = await itemDb.Items.ToListAsync();

                ViewData["counterList"] = counterList;
                ViewData["categoryList"] = categoryList;
                ViewData["itemList"] = itemList;

                return View("Index");
            }

            return View(user);
        }

        // Nothing
        // GET: Home/SignIn
        public ActionResult SignIn()
        {
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SignIn([Bind(Include = "username,password")] User user)
        {
            if (ModelState.IsValid)
            {
                List<User> userList = await db.Users.ToListAsync();
                foreach (User userObj in userList)
                {
                    if (user.username.Equals(userObj.username))
                    {
                        string encodedPassword = EncodePassword(user.password);
                        if (encodedPassword.Equals(userObj.password))
                        {
                            Session["loggedIn"] = true;
                            Session["User"] = userObj;
                            if (userObj.type.Equals("CUSTOMER"))
                            {
                                WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
                                WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();
                                WallnutDatabaseEntities6 itemDb = new WallnutDatabaseEntities6();

                                List<Counter> counterList = await counterDb.Counters.ToListAsync();
                                List<Category> categoryList = await categoryDb.Categories.ToListAsync();
                                List<Item> itemList = await itemDb.Items.ToListAsync();

                                ViewData["counterList"] = counterList;
                                ViewData["categoryList"] = categoryList;
                                ViewData["itemList"] = itemList;

                                return View("Index");
                                
                            }
                            else if (userObj.type.Equals("ADMIN"))
                            {
                                return View("AdminHome");
                            }
                            else if (userObj.type.Equals("SALES_MANAGER"))
                            {
                                return View("SALES_MANAGER_HOME");
                            }
                            else if (userObj.type.Equals("COUNTER_MANAGER"))
                            {
                                return View("COUNTER_MANAGER_HOME");
                            }
                            else if (userObj.type.Equals("SALES_PERSON"))
                            {
                                return View("SALES_PERSON_HOME");
                            }
                            else if (userObj.type.Equals("DELIVERY_PERSON"))
                            {
                                return View("DELIVERY_PERSON_HOME");
                            }
                            else
                            {
                                return View("Error");
                            }
                        }
                        else
                        {
                            ViewData["Status"] = "User credentials invalid!!!";
                            return View("SignIn");
                        }
                    }
                }

                ViewData["Status"] = "User credentials invalid!!!";
                return View("SignIn");
            }

            return View(user);
        }

        
        // GET: Home/Logout
        public ActionResult Logout()
        {
            Session.Abandon();

            return RedirectToAction("Index");
        }

        // Nothing
        public ActionResult About()
        {
            ViewBag.Message = "Our Company.";

            return View();
        }

        // Nothing
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        // Nothing
        // this function is used to encrypt password 
        public static string EncodePassword(string password)
        {
            try
            {
                byte[] encData_byte = new byte[password.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(password);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        }
    }
}