﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Wallnut_SuperMarket.Models;

namespace Wallnut_SuperMarket.Controllers
{
    public class OrderItemsController : Controller
    {
        private WallnutDatabaseEntities12 db = new WallnutDatabaseEntities12();

        // SALES_PERSON
        // Get: OrderItems/MarkPrepared/5
        public async Task<ActionResult> MarkPrepared(int? id)
        {
            OrderItem orderItemFrmDb = await db.OrderItems.FindAsync(id);
            orderItemFrmDb.preparedStatus = "READY";
            db.Entry(orderItemFrmDb).State = EntityState.Modified;
            await db.SaveChangesAsync();

            WallnutDatabaseEntities6 itemDb = new WallnutDatabaseEntities6();

            List<Item> itemList = await itemDb.Items.ToListAsync();
            List<OrderItem> orderItemList = await db.OrderItems.ToListAsync();
            List<OrderItem> preparedItems = new List<OrderItem>();
            List<OrderItem> unPreparedItems = new List<OrderItem>();

            foreach (OrderItem orderItem in orderItemList)
            {
                if (orderItem.preparedStatus.Equals("READY"))
                {
                    preparedItems.Add(orderItem);
                }
                else
                {
                    unPreparedItems.Add(orderItem);
                }
            }

            orderItemList = new List<OrderItem>();
            orderItemList.AddRange(unPreparedItems);
            orderItemList.AddRange(preparedItems);
            

            WallnutDatabaseEntities10 orderdb = new WallnutDatabaseEntities10();
            ViewData["orderList"] = await orderdb.Orders.ToListAsync();

            ViewData["orderItemList"] = orderItemList;
            ViewData["itemList"] = itemList;
            return View("PrepareItems");
        }

        // SALES_PERSON
        // Get: OrderItems/PepareItems
        public async Task<ActionResult> PrepareItems()
        {
            WallnutDatabaseEntities6 itemDb = new WallnutDatabaseEntities6();

            List<Item> itemList = await itemDb.Items.ToListAsync();
            List<OrderItem> orderItemList = await db.OrderItems.ToListAsync();
            List<OrderItem> preparedItems = new List<OrderItem>();
            List<OrderItem> unPreparedItems = new List<OrderItem>();

            foreach (OrderItem orderItem in orderItemList)
            {
                if (orderItem.preparedStatus.Equals("READY"))
                {
                    preparedItems.Add(orderItem);
                }
                else
                {
                    unPreparedItems.Add(orderItem);
                }
            }

            orderItemList = new List<OrderItem>();
            orderItemList.AddRange(unPreparedItems);
            orderItemList.AddRange(preparedItems);

            WallnutDatabaseEntities10 orderdb = new WallnutDatabaseEntities10();
            ViewData["orderList"] = await orderdb.Orders.ToListAsync();

            ViewData["orderItemList"] = orderItemList;
            ViewData["itemList"] = itemList;
            return View();
        }

        // Admin
        // GET: OrderItems
        public async Task<ActionResult> Index()
        {
            return View(await db.OrderItems.ToListAsync());
        }

        // Admin
        // GET: OrderItems/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderItem orderItem = await db.OrderItems.FindAsync(id);
            if (orderItem == null)
            {
                return HttpNotFound();
            }
            return View(orderItem);
        }

        // Admin
        // GET: OrderItems/Create
        public ActionResult Create()
        {
            return View();
        }

        // Admin
        // POST: OrderItems/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,orderId,itemId,qty,total,preparedStatus")] OrderItem orderItem)
        {
            if (ModelState.IsValid)
            {
                db.OrderItems.Add(orderItem);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(orderItem);
        }

        // Admin
        // GET: OrderItems/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderItem orderItem = await db.OrderItems.FindAsync(id);
            if (orderItem == null)
            {
                return HttpNotFound();
            }
            return View(orderItem);
        }

        // Admin
        // POST: OrderItems/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,orderId,itemId,qty,total,preparedStatus")] OrderItem orderItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(orderItem).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(orderItem);
        }

        // Admin
        // GET: OrderItems/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderItem orderItem = await db.OrderItems.FindAsync(id);
            if (orderItem == null)
            {
                return HttpNotFound();
            }
            return View(orderItem);
        }

        // Admin
        // POST: OrderItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            OrderItem orderItem = await db.OrderItems.FindAsync(id);
            db.OrderItems.Remove(orderItem);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
