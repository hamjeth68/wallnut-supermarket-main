﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Wallnut_SuperMarket.Models;

namespace Wallnut_SuperMarket.Controllers
{
    public class OrdersController : Controller
    {
        private WallnutDatabaseEntities10 db = new WallnutDatabaseEntities10();

        // Delivery person
        // Get: Orders/DeliveryOrders
        public async Task<ActionResult> MarkReady(int? id)
        {
            Order order = await db.Orders.FindAsync(id);

            order.readyStatus = "READY";
            db.Entry(order).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return RedirectToAction("ReadyOrders");
        }

        // COUNTER_MANAGER
        // Get: Orders/ReadyOrders
        public async Task<ActionResult> ReadyOrders() {
            WallnutDatabaseEntities12 orderItemDb = new WallnutDatabaseEntities12();
            WallnutDatabaseEntities userdb = new WallnutDatabaseEntities();

            List<Order> orderList = await db.Orders.OrderByDescending(o => o.id).ToListAsync();
            List<OrderItem> orderItemList = await orderItemDb.OrderItems.ToListAsync();
            List<User> userList = await userdb.Users.ToListAsync();
            List<Order> deliveredOrderList = new List<Order>();
            List<Order> notDeliveredOrderList = new List<Order>();

            foreach (Order order in orderList)
            {
                if (order.readyStatus.Equals("READY") || order.readyStatus.Equals("DELIVERED") || order.readyStatus.Equals("CANCELED"))
                {
                    deliveredOrderList.Add(order);
                }
                else
                {
                    notDeliveredOrderList.Add(order);
                }
            }

            ViewData["deliveredOrderList"] = deliveredOrderList;
            ViewData["notDeliveredOrderList"] = notDeliveredOrderList;
            ViewData["orderItemList"] = orderItemList;
            ViewData["userList"] = userList;

            return View();
        }

        // Delivery person
        // Get: Orders/DeliveryOrders
        public async Task<ActionResult> MarkDeliver(int? id)
        {
            Order order = await db.Orders.FindAsync(id);

            order.readyStatus = "DELIVERED";
            db.Entry(order).State = EntityState.Modified;
            await db.SaveChangesAsync();

            User user = (User)Session["user"];
            if (user.type.Equals("COUNTER_MANAGER"))
            {
                return RedirectToAction("ReadyOrders");
            }
            else
            {
                return RedirectToAction("DeliveryOrders");
            }
        }

        // Delivery person
        // GET: Orders/DeliveryOrders
        public async Task<ActionResult> DeliveryOrders()
        {
            WallnutDatabaseEntities11 orderDeliveryPersondb = new WallnutDatabaseEntities11();
            WallnutDatabaseEntities userdb = new WallnutDatabaseEntities();

            List<Order> originalOrderList = new List<Order>();
            List<Order> deliverdOrderList = new List<Order>();
            List<Order> notDeliverdOrderList = new List<Order>();
            
            List<Order> orderList = await db.Orders.OrderByDescending(o => o.id).ToListAsync();
            List<User> userList = await userdb.Users.ToListAsync();

            List<OrderDeliveryPerson> orderDeliveryPersonList = await orderDeliveryPersondb.OrderDeliveryPersons.ToListAsync();
            User user = (User)Session["user"];

            foreach(OrderDeliveryPerson orderDeliveryPersonObj in orderDeliveryPersonList)
            {
                if(orderDeliveryPersonObj.deliveryPersonId == user.id)
                {
                    foreach (Order orderObj in orderList)
                    {
                        if(orderObj.bill == orderDeliveryPersonObj.bill)
                        {
                            if (orderObj.readyStatus.Equals("DELIVERED"))
                            {
                                deliverdOrderList.Add(orderObj);
                            }
                            else if(orderObj.readyStatus.Equals("READY"))
                            {
                                notDeliverdOrderList.Add(orderObj);
                            }
                        }
                    }
                }
            }
            
            originalOrderList.AddRange(notDeliverdOrderList);
            originalOrderList.AddRange(deliverdOrderList);

            ViewData["orderList"] = originalOrderList;
            ViewData["userList"] = userList;

            return View();
        }

        // Users
        // GET: Orders/CancelOrder/01
        public async Task<ActionResult> CancelOrder(int? id)
        {
            WallnutDatabaseEntities9 billDb = new WallnutDatabaseEntities9();
            WallnutDatabaseEntities14 walletdb = new WallnutDatabaseEntities14();
            WallnutDatabaseEntities6 itemDb = new WallnutDatabaseEntities6();
            WallnutDatabaseEntities12 orderItemDb = new WallnutDatabaseEntities12();

            // Updating Wallet
            User user = (User)Session["user"];
            
            Order orderFrmDb = await db.Orders.FindAsync(id);
            List<Bill> billList = (List<Bill>)await billDb.Bills.ToListAsync();
            Bill originalBill = new Bill();
            foreach (Bill billObj in billList) { 
                if (billObj.id == orderFrmDb.bill)
                {
                    originalBill = billObj;
                }
            }

            List<Wallet> walletList = (List<Wallet>)await walletdb.Wallets.ToListAsync();
            Wallet originalWallet = new Wallet();
            foreach (Wallet walletObj in walletList)
            {
                if (walletObj.userId == user.id)
                {
                    originalWallet = walletObj;
                }
            }

            originalWallet.amount = originalWallet.amount + originalBill.payment;
            walletdb.Entry(originalWallet).State = EntityState.Modified;
            await walletdb.SaveChangesAsync();

            // Changing order status
            orderFrmDb.readyStatus = "CANCELED";
            db.Entry(orderFrmDb).State = EntityState.Modified;
            await db.SaveChangesAsync();

            // Changing stock status
            List<Item> itemList = (List<Item>)await itemDb.Items.ToListAsync();
            List<OrderItem> orderItemList = (List<OrderItem>)await orderItemDb.OrderItems.ToListAsync();

            foreach(OrderItem orderItemObj in orderItemList)
            {
                if (orderItemObj.orderId == orderFrmDb.id)
                {
                   // ordered item
                   foreach(Item itemObj in itemList)
                   {
                        if(itemObj.id == orderItemObj.itemId)
                        {
                            itemObj.stock = itemObj.stock + orderItemObj.qty;
                            itemDb.Entry(itemObj).State = EntityState.Modified;
                            await itemDb.SaveChangesAsync();
                        }
                   }
                }
            }


            return RedirectToAction("PastOrders");
        }

        // Users
        // Get: Orders/PastOrders
        public async Task<ActionResult> PastOrders()
        {
            WallnutDatabaseEntities9 billDb = new WallnutDatabaseEntities9();
            WallnutDatabaseEntities6 itemDb = new WallnutDatabaseEntities6();
            WallnutDatabaseEntities12 orderItemDb = new WallnutDatabaseEntities12();
            WallnutDatabaseEntities13 shippingDb = new WallnutDatabaseEntities13();
            WallnutDatabaseEntities userDb = new WallnutDatabaseEntities();
            WallnutDatabaseEntities11 orderDeliveryPersonDb = new WallnutDatabaseEntities11();

            User user = (User)Session["user"];

            List<Order> orderList = await db.Orders.OrderByDescending(o => o.id).ToListAsync();
            List<Order> orderListOfCustomer = new List<Order>();

            foreach (Order orderObj in orderList)
            {
                if (orderObj.customerId == user.id)
                {
                    orderListOfCustomer.Add(orderObj);
                }
            }

            List<Bill> billList = await billDb.Bills.ToListAsync();
            List<OrderItem> orderItemList = await orderItemDb.OrderItems.ToListAsync();
            List<Item> itemList = await itemDb.Items.ToListAsync();
            List<Shipping> shippingList = await shippingDb.Shippings.ToListAsync();
            List<User> userList = await userDb.Users.ToListAsync();
            List<OrderDeliveryPerson> orderDeliveryPersonList = await orderDeliveryPersonDb.OrderDeliveryPersons.ToListAsync();

            ViewData["orderListOfCustomer"] = orderListOfCustomer;
            ViewData["billList"] = billList;
            ViewData["orderItemList"] = orderItemList;
            ViewData["itemList"] = itemList;
            ViewData["shippingList"] = shippingList;
            ViewData["userList"] = userList;
            ViewData["orderDeliveryPersonList"] = orderDeliveryPersonList;

            return View();
        }

        // Admin
        // GET: Orders
        public async Task<ActionResult> Index()
        {
            return View(await db.Orders.ToListAsync());
        }

        // Admin
        // GET: Orders/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // Admin
        // GET: Orders/Create
        public ActionResult Create()
        {
            return View();
        }

        // Admin
        // POST: Orders/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,bill,customerId,pickupStatus,date_time,readyStatus")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Orders.Add(order);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(order);
        }

        // Admin
        // GET: Orders/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // Admin
        // POST: Orders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,bill,customerId,pickupStatus,date_time,readyStatus")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        // Admin
        // GET: Orders/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // Admin
        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Order order = await db.Orders.FindAsync(id);
            db.Orders.Remove(order);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
