﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Wallnut_SuperMarket.Models;

namespace Wallnut_SuperMarket.Controllers
{
    public class UsersController : Controller
    {
        private WallnutDatabaseEntities db = new WallnutDatabaseEntities();

        // Admin
        // GET: Users
        public async Task<ActionResult> Index()
        {
            if (Session["loggedIn"] != null)
            {
                var loggedIn = (Boolean)Session["loggedIn"];
                var user = (Wallnut_SuperMarket.Models.User)Session["User"];

                if (user.type.Equals("ADMIN") && loggedIn == true)
                {
                    return View(await db.Users.ToListAsync());
                }
                else
                {
                    return Redirect("../Home/SignIn");
                }
            }
            else
            {
                return Redirect("../Home/SignIn");
            }
        }

        // Admin
        // GET: Users/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // Admin
        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // Admin
        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,username,password,email,phone,address,type")] User user)
        {
            List<User> userList = await db.Users.ToListAsync();
            foreach (User userObj in userList)
            {
                if (user.username.Equals(userObj.username))
                {
                    ViewData["Status"] = "Username " + userObj.username + " is already taken!!!";
                    return View("Create");
                }
            }

            string encodedPassword = EncodePassword(user.password);
            user.password = encodedPassword;
            db.Users.Add(user);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // Admin
        // GET: Users/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // Admin
        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,username,password,email,phone,address,type")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // Admin
        // GET: Users/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // Admin
        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            User user = await db.Users.FindAsync(id);
            db.Users.Remove(user);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // Nothing
        // this function is used to encrypt password 
        public static string EncodePassword(string password)
        {
            try
            {
                byte[] encData_byte = new byte[password.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(password);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        }
    }
}
