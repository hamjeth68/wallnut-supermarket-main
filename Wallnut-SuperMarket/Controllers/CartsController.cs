﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Wallnut_SuperMarket.Models;

//commit changes


namespace Wallnut_SuperMarket.Controllers
{
    public class CartsController : Controller
    {
        private WallnutDatabaseEntities8 db = new WallnutDatabaseEntities8();

        // User
        // Get:Carts/Checkout
        public async Task<ActionResult> Checkout()
        {
            WallnutDatabaseEntities14 walletdb = new WallnutDatabaseEntities14();
            WallnutDatabaseEntities6 itemDb = new WallnutDatabaseEntities6();

            List<Cart> cartList = (List<Cart>)Session["cartList"];
            float walletPrice = 0;

            List<Wallet> walletList = await walletdb.Wallets.ToListAsync();
            User user = (User)Session["user"];

            foreach (Wallet wallet in walletList)
            {
                if (wallet.userId == user.id)
                {
                    walletPrice = (float) wallet.amount;
                }
            }

            float totalBill = 0;
            foreach (Cart cart in cartList)
            {
                float total = 0;
                int itemId = (int)cart.itemId;
                Item item = await itemDb.Items.FindAsync(itemId);
                total = (float)item.rating * (float)cart.qty;
                totalBill = totalBill + total;
            }

            ViewData["payment"] = totalBill;
            ViewData["wallet"] = walletPrice;
            return View();
        }

        public async Task<ActionResult> receipt()
        {
            ViewData["state"] = "ERROR";

            return View();
        }

        // User
        // POST: Carts/Checkout/
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Checkout([Bind(Include = "pickStatus,address,distance,payMethod,cardNum,cvv,walletCash,message,totaly")] string pickStatus, string address, string distance, string payMethod, string cardNum, string cvv, string walletCash, string message, string totaly)
        {
            WallnutDatabaseEntities6 itemDb = new WallnutDatabaseEntities6();
            WallnutDatabaseEntities9 billDb = new WallnutDatabaseEntities9();
            WallnutDatabaseEntities10 orderDb = new WallnutDatabaseEntities10();
            WallnutDatabaseEntities11 orderDeliveryPersonDb = new WallnutDatabaseEntities11();
            WallnutDatabaseEntities userDb = new WallnutDatabaseEntities();
            WallnutDatabaseEntities12 orderItemDb = new WallnutDatabaseEntities12();
            WallnutDatabaseEntities13 shippingDb = new WallnutDatabaseEntities13();
            WallnutDatabaseEntities14 walletdb = new WallnutDatabaseEntities14();

            // Check payment details
            if (payMethod.Equals("CARD"))
            {
                // card dummy validation
                if (cardNum.Length == 12 && (cvv.Length == 3 || cvv.Length == 4))
                {
                    // No error
                }
                else
                {
                    // error
                    ViewData["Status"] = "Card number or cvv invalid";
                    List<Cart> cartList = (List<Cart>)Session["cartList"];
                    float walletPrice = 0;

                    List<Wallet> walletList = await walletdb.Wallets.ToListAsync();
                    User user = (User)Session["user"];

                    foreach (Wallet wallet in walletList)
                    {
                        if (wallet.userId == user.id)
                        {
                            walletPrice = (float)wallet.amount;
                        }
                    }

                    float totalBill = 0;
                    foreach (Cart cart in cartList)
                    {
                        float total = 0;
                        int itemId = (int)cart.itemId;
                        Item item = await itemDb.Items.FindAsync(itemId);
                        total = (float)item.rating * (float)cart.qty;
                        totalBill = totalBill + total;
                    }

                    ViewData["payment"] = totalBill;
                    ViewData["wallet"] = walletPrice;
                    return View("Checkout");
                }
            }
            else
            {
                if (pickStatus.Equals("HOME_DELIVERY") && (address.Equals("") || distance.Equals(0) || (int)int.Parse(distance) > 4))
                {
                    // error
                    ViewData["Status"] = "Form details invalid";
                    List<Cart> cartList = (List<Cart>)Session["cartList"];
                    float walletPrice = 0;

                    List<Wallet> walletList = await walletdb.Wallets.ToListAsync();
                    User user = (User)Session["user"];

                    foreach (Wallet wallet in walletList)
                    {
                        if (wallet.userId == user.id)
                        {
                            walletPrice = (float)wallet.amount;
                        }
                    }

                    float totalBill = 0;
                    foreach (Cart cart in cartList)
                    {
                        float total = 0;
                        int itemId = (int)cart.itemId;
                        Item item = await itemDb.Items.FindAsync(itemId);
                        total = (float)item.rating * (float)cart.qty;
                        totalBill = totalBill + total;
                    }

                    ViewData["payment"] = totalBill;
                    ViewData["wallet"] = walletPrice;
                    return View("Checkout");
                }
                else
                {
                    float totalX = (float)(float.Parse(totaly));
                    float walletCashX = (float)(float.Parse(walletCash));
                    if (totalX > walletCashX)
                    {
                        // error
                        ViewData["Status"] = "Wallet price insufficent";
                        List<Cart> cartList = (List<Cart>)Session["cartList"];
                        float walletPrice = 0;

                        List<Wallet> walletList = await walletdb.Wallets.ToListAsync();
                        User user = (User)Session["user"];

                        foreach (Wallet wallet in walletList)
                        {
                            if (wallet.userId == user.id)
                            {
                                walletPrice = (float)wallet.amount;
                            }
                        }

                        float totalBill = 0;
                        foreach (Cart cart in cartList)
                        {
                            float total = 0;
                            int itemId = (int)cart.itemId;
                            Item item = await itemDb.Items.FindAsync(itemId);
                            total = (float)item.rating * (float)cart.qty;
                            totalBill = totalBill + total;
                        }

                        ViewData["payment"] = totalBill;
                        ViewData["wallet"] = walletPrice;
                        return View("Checkout");
                    }
                }
            }

            if (Session["cartList"] != null)
            {
                List<Cart> cartList = (List<Cart>)Session["cartList"];
                // reduce stock
                foreach (Cart cart in cartList)
                {
                    int itemId = (int)cart.itemId;
                    Item item = await itemDb.Items.FindAsync(itemId);
                    item.stock = item.stock - cart.qty;
                    itemDb.Entry(item).State = EntityState.Modified;
                    await itemDb.SaveChangesAsync();
                }

                if (pickStatus == "HOME_DELIVERY")
                {
                    if((int)int.Parse(distance) > 4 || (int)int.Parse(distance) == 0 || address.Equals("")){
                        // error
                        // error
                        ViewData["Status"] = "Form details invalid";
                        List<Cart> xcartList = (List<Cart>)Session["cartList"];
                        float walletPrice = 0;

                        List<Wallet> walletList = await walletdb.Wallets.ToListAsync();
                        User xuser = (User)Session["user"];

                        foreach (Wallet wallet in walletList)
                        {
                            if (wallet.userId == xuser.id)
                            {
                                walletPrice = (float)wallet.amount;
                            }
                        }

                        float xtotalBill = 0;
                        foreach (Cart cart in xcartList)
                        {
                            float total = 0;
                            int itemId = (int)cart.itemId;
                            Item item = await itemDb.Items.FindAsync(itemId);
                            total = (float)item.rating * (float)cart.qty;
                            xtotalBill = xtotalBill + total;
                        }

                        ViewData["payment"] = xtotalBill;
                        ViewData["wallet"] = walletPrice;
                        return View("Checkout");
                    }

                    User user = (User)Session["user"];

                    // save bill
                    Bill bill = new Bill();

                    float deliveryCharge = 0;
                    float totalBill = 0;
                    deliveryCharge = (float)(float.Parse(distance) * 50.0);

                    foreach (Cart cart in cartList)
                    {
                        float total = 0;
                        int itemId = (int)cart.itemId;
                        Item item = await itemDb.Items.FindAsync(itemId);
                        total = (float) item.rating * (float) cart.qty;
                        totalBill = totalBill + total;
                    }

                    bill.id = 0;
                    bill.payment = deliveryCharge + totalBill;

                    billDb.Bills.Add(bill);
                    await billDb.SaveChangesAsync();

                    // save order
                    DateTime currentTime = DateTime.Now;
                    DateTime x60MinsLater = currentTime.AddMinutes(60);

                    Order order = new Order();

                    order.id = 0;
                    order.bill = bill.id;
                    order.customerId = user.id;
                    order.pickupStatus = "HOME_DELIVERY";
                    order.readyStatus = "NOT_READY";
                    order.date_time = x60MinsLater;

                    orderDb.Orders.Add(order);
                    await orderDb.SaveChangesAsync();

                    // save OrderDeliveryPerson
                    int deliveryPersonId = 0;
                    OrderDeliveryPerson orderDeliveryPerson = new OrderDeliveryPerson();

                    // pick a delivery person
                    List<User> userList = await userDb.Users.ToListAsync();
                    List<User> deliveryPersonList = new List<User>();

                    foreach (User userObj in userList)
                    {
                        if (userObj.type.Equals("DELIVERY_PERSON"))
                        {
                            deliveryPersonList.Add(userObj);
                        }
                    }

                    int length = deliveryPersonList.Count();
                    Random _random = new Random();
                    int deliveryPersonIndex = _random.Next(0, length);
                    User selectedDeliverPerson = deliveryPersonList[deliveryPersonIndex];
                    deliveryPersonId = selectedDeliverPerson.id;

                    orderDeliveryPerson.id = 0;
                    orderDeliveryPerson.bill = bill.id;
                    orderDeliveryPerson.deliveryPersonId = deliveryPersonId;

                    orderDeliveryPersonDb.OrderDeliveryPersons.Add(orderDeliveryPerson);
                    await orderDeliveryPersonDb.SaveChangesAsync();

                    // save order items
                    foreach (Cart cartObj in cartList)
                    {
                        OrderItem orderItem = new OrderItem();
                        Item item = await itemDb.Items.FindAsync(cartObj.itemId);

                        orderItem.id = 0;
                        orderItem.orderId = order.id;
                        orderItem.itemId = cartObj.itemId;
                        orderItem.qty = cartObj.qty;
                        orderItem.total = (float) cartObj.qty * item.rating;
                        orderItem.preparedStatus = "NOT_READY";

                        orderItemDb.OrderItems.Add(orderItem);
                        await orderItemDb.SaveChangesAsync();
                    }

                    // save shipping
                    Shipping shipping = new Shipping();

                    shipping.id = 0;
                    shipping.address = address;
                    shipping.distance = (int) int.Parse(distance);
                    shipping.total = deliveryCharge;
                    shipping.orderId = order.id;

                    shippingDb.Shippings.Add(shipping);
                    await shippingDb.SaveChangesAsync();

                    // Wallet price reduce
                    if (payMethod.Equals("WALLET"))
                    {
                        List<Wallet> walletList = await walletdb.Wallets.ToListAsync();
                        Wallet walletFromDb = new Wallet();
                        foreach (Wallet wallet in walletList)
                        {
                            if (wallet.userId == user.id)
                            {
                                walletFromDb = wallet;    
                            }
                        }

                        walletFromDb.amount = walletFromDb.amount - bill.payment;
                        walletdb.Entry(walletFromDb).State = EntityState.Modified;
                        await walletdb.SaveChangesAsync();
                    }

                    ViewData["bill"] = bill;
                    ViewData["deliverPerson"] = selectedDeliverPerson;
                    ViewData["expectedTime"] = x60MinsLater;
                    ViewData["totalOrder"] = totalBill;
                    ViewData["deliveryCharge"] = deliveryCharge;
                    ViewData["order"] = order;

                    ViewData["state"] = "OK";

                    Session["cartList"] = null;

                    return View("receipt");
                }
                else
                {
                    // BILL, ORDER, ORDER ITEMS, 
                    // Not a home delivery
                    User user = (User)Session["user"];

                    // save bill
                    Bill bill = new Bill();

                    float deliveryCharge = 0;
                    float totalBill = 0;

                    foreach (Cart cart in cartList)
                    {
                        float total = 0;
                        int itemId = (int)cart.itemId;
                        Item item = await itemDb.Items.FindAsync(itemId);
                        total = (float)item.rating * (float)cart.qty;
                        totalBill = totalBill + total;
                    }

                    bill.id = 0;
                    bill.payment = deliveryCharge + totalBill;

                    billDb.Bills.Add(bill);
                    await billDb.SaveChangesAsync();

                    // save order
                    DateTime currentTime = DateTime.Now;
                    DateTime x60MinsLater = currentTime.AddMinutes(60);

                    Order order = new Order();

                    order.id = 0;
                    order.bill = bill.id;
                    order.customerId = user.id;
                    order.pickupStatus = "PICK_UP";
                    order.readyStatus = "NOT_READY";
                    order.date_time = x60MinsLater;

                    orderDb.Orders.Add(order);
                    await orderDb.SaveChangesAsync();

                    // save order items
                    foreach (Cart cartObj in cartList)
                    {
                        OrderItem orderItem = new OrderItem();
                        Item item = await itemDb.Items.FindAsync(cartObj.itemId);

                        orderItem.id = 0;
                        orderItem.orderId = order.id;
                        orderItem.itemId = cartObj.itemId;
                        orderItem.qty = cartObj.qty;
                        orderItem.total = (float)cartObj.qty * item.rating;
                        orderItem.preparedStatus = "NOT_READY";

                        orderItemDb.OrderItems.Add(orderItem);
                        await orderItemDb.SaveChangesAsync();
                    }

                    // Wallet price reduce
                    if (payMethod.Equals("WALLET"))
                    {
                        List<Wallet> walletList = await walletdb.Wallets.ToListAsync();
                        Wallet walletFromDb = new Wallet();
                        foreach (Wallet wallet in walletList)
                        {
                            if (wallet.userId == user.id)
                            {
                                walletFromDb = wallet;
                            }
                        }

                        walletFromDb.amount = walletFromDb.amount - bill.payment;
                        walletdb.Entry(walletFromDb).State = EntityState.Modified;
                        await walletdb.SaveChangesAsync();
                    }

                    ViewData["bill"] = bill;
                    ViewData["expectedTime"] = x60MinsLater;
                    ViewData["totalOrder"] = totalBill;
                    ViewData["deliveryCharge"] = deliveryCharge;
                    ViewData["order"] = order;

                    ViewData["state"] = "OK";

                    Session["cartList"] = null;

                    return View("receipt");
                }
            }

            ViewData["state"] = "ERROR";

            return View("receipt");
        }

        // User
        //Get:Carts/View
        public async Task<ActionResult> ViewCart()
        {
            WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
            WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();
            WallnutDatabaseEntities6 itemDb = new WallnutDatabaseEntities6();

            List<Counter> counterList = await counterDb.Counters.ToListAsync();
            List<Category> categoryList = await categoryDb.Categories.ToListAsync();
            List<Item> itemList = await itemDb.Items.ToListAsync();

            ViewData["counterList"] = counterList;
            ViewData["categoryList"] = categoryList;
            ViewData["itemList"] = itemList;

            return View();
        }

        // Admin
        // GET: Carts
        public async Task<ActionResult> Index()
        {
            return View(await db.Carts.ToListAsync());
        }

        // Admin
        // GET: Carts/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart cart = await db.Carts.FindAsync(id);
            if (cart == null)
            {
                return HttpNotFound();
            }
            return View(cart);
        }

        // Admin
        // GET: Carts/Create
        public ActionResult Create()
        {
            return View();
        }

        // Admin
        // POST: Carts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,itemId,qty")] Cart cart)
        {
            if (ModelState.IsValid)
            {
                db.Carts.Add(cart);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(cart);
        }

        // Admin
        // GET: Carts/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart cart = await db.Carts.FindAsync(id);
            if (cart == null)
            {
                return HttpNotFound();
            }
            return View(cart);
        }

        // Admin
        // POST: Carts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,itemId,qty")] Cart cart)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cart).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(cart);
        }

        // Admin
        // GET: Carts/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart cart = await db.Carts.FindAsync(id);
            if (cart == null)
            {
                return HttpNotFound();
            }
            return View(cart);
        }

        // User
        // GET: Carts/Delete/5
        public async Task<ActionResult> DeleteCart(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            List<Cart> cartList = (List<Cart>)Session["cartList"];
            List<Cart> newCartList = new List<Cart>();

            foreach (Cart cartObj in cartList)
            {
                if (cartObj.id != id)
                {
                    newCartList.Add(cartObj);
                }
            }

            Session["cartList"] = newCartList;

            WallnutDatabaseEntities2 counterDb = new WallnutDatabaseEntities2();
            WallnutDatabaseEntities4 categoryDb = new WallnutDatabaseEntities4();
            WallnutDatabaseEntities6 itemDb = new WallnutDatabaseEntities6();

            List<Counter> counterList = await counterDb.Counters.ToListAsync();
            List<Category> categoryList = await categoryDb.Categories.ToListAsync();
            List<Item> itemList = await itemDb.Items.ToListAsync();

            ViewData["counterList"] = counterList;
            ViewData["categoryList"] = categoryList;
            ViewData["itemList"] = itemList;

            return View("ViewCart");
        }

        // Admin
        // POST: Carts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Cart cart = await db.Carts.FindAsync(id);
            db.Carts.Remove(cart);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
